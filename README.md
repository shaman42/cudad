# cudAD: simple automatic differentiation to be used on the CPU and in CUDA kernels

[![Build&Test Status](https://gitlab.com/shaman42/cudad/badges/master/pipeline.svg)](https://gitlab.com/shaman42/cudad/-/commits/master)



Assume you have the following function:

```C++
#include <cmath>
float demo(float x, float y) {
    using namespace std; //for sin, tan, pow, abs
    float t1 = x*y + 5*sin(x);
    float t2 = tan(y) - pow(abs(x), 2*y);
    return t1+t2;
}
```

And now you want to compute the derivative of `z=demo(x,y)` with regard to x and y. But you are too lazy to compute the derivatives yourself. What do you do?

Automatic differentiation comes to your rescue!

Step 1: Define the function using templates so that you can pass any datatype to it

```C++
#include <cmath>
template<typename T1, typename T2>
auto demo(const T1& x, const T2& y) {
    using namespace std; //for sin, tan, pow, abs
    auto t1 = x*y + 5*sin(x);
    auto t2 = tan(y) - pow(abs(x), 2*y);
    return t1+t2;
}
```

You can still call the function as above (without derivatives), but now you can also compute derivatives:

```C++
#include <cudAD/forward.h>

typedef cudAD::fvar<float, 1> d1; //carries one derivative
typedef cudAD::fvar<float, 2> d2; //carries two derivatives

//derivative of z w.r.t. x
auto result = demo(d1::input<0>(x), y);
float z = result.value();
float dzdx = result.derivative<0>(); //the derivative

//derivative of z w.r.t. y
auto result = demo(x, d1::input<0>(x));
float z = result.value();
float dzdy = result.derivative<0>(); //the derivative

//derivatives for both inputs at the same time
auto result = demo(d2::input<0>(x), d2::input<1>(x));
float z = result.value();
float dzdx = result.derivative<0>(); //derivative of z w.r.t. x
float dzdy = result.derivative<1>(); //derivative of z w.r.t. x
```

How does it work?

cudAD uses the so-called *Forward Differentiation Method*. 
The idea is the following: Assume you want to compute the derivatives of some parameter p in your program. For a variable x, you already know the derivate dx/dp up to that point. 
Now for a known function y=f(x), we know the derivative df/dx. With the help of the chain rule, we can thus compute dy/dp = df/dx * dx/dp.

This is the idea that is exploited by cudAD:

- The derivatives of the current variable with respect to the input parameters is stored in `cudAD::fvar<T,N>`, where `T` is the datatype, e.g. float or double, and `N` is the number of derivatives that are traced.
- Tracing starts when a parameter to derive for is first used in the algorithm: dp/dp=1 by definition.
  This is represented by `fvar<T,N>::input<0>(p)` where the template parameter 0 defines the index into the array of derivatives.
  (Note: there is also the alternative where the index is specified as a runtime parameter)
- Then cudAD defines operator overloadings for all common unary and binary math operations. Hence, writing e.g. `y=sin(x)` with x being a `fvar` automatically propagates the derivatives.
  For binary functions, cudAD supports mixed types: For `x*y` as an example, either x, y or both can be an `fvar`, the other variable a regular float or double. 

## When to use

- You want to trace a few derivatives through a long chain of operations with hundreds to thousands of operations, including loops and control flow? --> use cudAD
- You want to compute derivatives on the GPU where you have no heap for storing a stack of intermediate values as required by the adjoint method --> use cudAD
- You have relatively few operations but thousands of parameters to differentiate, e.g. training neural networks? --> Better use the adjoint method with frameworks like PyTorch or Tensorflow.



## Doxygen

The documentation can be found under [https://shaman42.gitlab.io/cudad/](https://shaman42.gitlab.io/cudad/)

## License
cudAD is shipped under the permissive [MIT](https://choosealicense.com/licenses/mit/) license.

## Bug reports
If you find bugs in the library, feel free to open an issue. I will continue to use this library in future projects and therefore continue to improve and extend this library. Of course, pull requests are more than welcome.

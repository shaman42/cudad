#pragma once
#ifndef __CUDAD_MACROS_H__
#define __CUDAD_MACROS_H__

#ifndef CUDA_NO_HOST
#include <cuda_runtime.h>
#endif

/*
 * This file contains global macros and type definitions used everywhere
 */

#ifndef CUDAD_NAMESPACE
 /**
  * \brief The namespace of the library
  */
#define CUDAD_NAMESPACE ::cudAD::
#endif

#ifndef CUDAD_NAMESPACE_BEGIN
  /**
   * \brief Defines the namespace in which everything of CUDAD lives in
   */
#define CUDAD_NAMESPACE_BEGIN namespace cudAD {
#endif

#ifndef CUDAD_NAMESPACE_END
   /**
	* \brief Closes the namespace opened with CUDAD_NAMESPACE_BEGIN
	*/
#define CUDAD_NAMESPACE_END }
#endif

/**
 * \brief Define this macro in a class that should not be copyable or assignable
 * \param TypeName the name of the class
 */
#define CUDAD_DISALLOW_COPY_AND_ASSIGN(TypeName)\
	TypeName(const TypeName&) = delete;      \
    void operator=(const TypeName&) = delete

#define CUDAD_STR_DETAIL(x) #x
#define CUDAD_STR(x) CUDAD_STR_DETAIL(x)

#define CUDAD_CALL __host__ __device__ __inline__ constexpr
#define CUDAD_CALL_NOCONSTEXPR __host__ __device__ __inline__


#endif

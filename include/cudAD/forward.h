#pragma once
#ifndef __CUDAD_FORWARD_H__
#define __CUDAD_FORWARD_H__

#ifndef CUDA_NO_HOST
#include <cassert>
#include <cmath>
#include <ostream>
#include <stdexcept>
#else
#include "cmath_cuda.cuh"
#endif

#include "macros.h"
CUDAD_NAMESPACE_BEGIN

namespace internal
{

    //In CUDA-only mode, no host libraries are available.
    //Hence, duplicate type traits
    template <class, class>
    inline constexpr bool is_same_v = false; // determine whether arguments are the same type
    template <class _Ty>
    inline constexpr bool is_same_v<_Ty, _Ty> = true;

    template <class _Test, class _Ty>
    inline constexpr bool is_type_or_floating_v =
        is_same_v<_Ty, _Test> || is_same_v<float, _Test> ||
        is_same_v<double, _Test> || is_same_v<long double, _Test> ||
        is_same_v<int, _Test> || is_same_v<unsigned int, _Test> ||
        is_same_v<long, _Test> || is_same_v<unsigned long, _Test> ||
        is_same_v<long long, _Test> || is_same_v<unsigned long long, _Test>;

    template <bool _Test, class _Ty = void>
    struct enable_if {}; // no member "type" when !_Test

    template <class _Ty>
    struct enable_if<true, _Ty> { // type is _Ty for _Test
        using type = _Ty;
    };

    template <bool _Test, class _Ty = void>
    using enable_if_t = typename enable_if<_Test, _Ty>::type;

    template <class _Test, class _Ty>
    using enable_if_type_or_floating_t = typename enable_if<
        is_type_or_floating_v<_Test, _Ty>, int>::type;
	
    template<typename T, int N>
    class array
    {
    public:
        typedef array<T, N> Type;
        typedef T Scalar;
        static constexpr int Length = N;

    private:
        T data_[N];

    public:
        CUDAD_CALL array() : data_{0} {}
        CUDAD_CALL int size() const { return Length; }
        CUDAD_CALL const T& at(int i) const
        {
            if (i < 0 || i >= Length) throw std::logic_error("array<T,N>::at out of bounds");
            return data_[i];
        }
        CUDAD_CALL T& at(int i)
        {
            if (i < 0 || i >= Length) throw std::logic_error("array<T,N>::at out of bounds");
            return data_[i];
        }
        CUDAD_CALL const T& operator[](int i) const
        {
            assert(i >= 0 && i < Length);
            return data_[i];
        }
        CUDAD_CALL T& operator[](int i)
        {
            assert(i >= 0 && i < Length);
            return data_[i];
        }
        CUDAD_CALL T* data() { return data_; }
        CUDAD_CALL const T* data() const { return data_; }
    };

    //specialization so that gcc is happy and doesn't report
    //  error: too many initializers for 'float [0]'
    //for CUDAD_CALL array() : data_{0} {} above
    template<typename T>
    class array<T, 0>
    {
    public:
        typedef array<T, 0> Type;
        typedef T Scalar;
        static constexpr int Length = 0;

    public:
        CUDAD_CALL array() {}
        CUDAD_CALL int size() const { return Length; }
        CUDAD_CALL const T& at(int i) const
        {
            throw std::logic_error("Can't index an array of length zero");
        }
        CUDAD_CALL T& at(int i)
        {
            throw std::logic_error("Can't index an array of length zero");
        }
        CUDAD_CALL const T& operator[](int i) const
        {
            throw std::logic_error("Can't index an array of length zero");
        }
        CUDAD_CALL T& operator[](int i)
        {
            throw std::logic_error("Can't index an array of length zero");
        }
        CUDAD_CALL T* data()
        {
            throw std::logic_error("Can't index an array of length zero");
        }
        CUDAD_CALL const T* data() const
        {
            throw std::logic_error("Can't index an array of length zero");
        }
    };
}

/**
 * \brief (Immutable) Variable for forward-mode automatic differentiation.
 * Tracks the function value and the derivatives for 'N' input variables.
 *
 * Instances of this class should not be directly created.
 * The default constructor leaves the members uninitialized.
 *
 * Instead, for input variables use the static function \ref input<k>(T value).
 * This creates a variable for the input of index k (0 <= k < N).
 * The derivatives for this variable are tracked by subsequent calls.
 * This is done by initializing \c derivatives[k] with 1 and all others with zero.
 *
 * After the input variables are assembles, the overloaded operators
 * trace the derivatives through the computations.
 * 
 * \tparam T the scalar type of the function
 * \tparam N the number of derivatives to track
 * \tparam TDiff the scalar type to track the derivatives, usually just T
 */
template<typename T, int N, typename TDiff = T>
struct fvar
{
	typedef fvar<T, N, TDiff> Type;
    typedef T Scalar;
    typedef TDiff ScalarDiff;
    static constexpr int NumDerivatives = N;

private:
	T value_;
    internal::array<TDiff, N> derivatives_;

public:
    /**
     * \brief Default constructors, sets the value and derivative to zero
     */
    CUDAD_CALL fvar() : value_{ 0 } {}

    CUDAD_CALL fvar(const T& value, const internal::array<TDiff, N>& derivatives)
        : value_(value), derivatives_(derivatives)
    {}
    CUDAD_CALL fvar(const T& value, internal::array<TDiff, N>&& derivatives)
        : value_(value), derivatives_(std::move(derivatives))
    {}

    //TODO: do I need copy and move constructors and assignment?

public:
    /**
	 * \brief Creates a variable for the k-th input with the specified value.
	 * Derivatives for this input are tracked.
	 * \param value the input variable
	 * \param k the index of the variable, 0<=k<N
	 * \param deriv the initial value for the derivative. Usually just 1 (the default),
	 *   unless you use a special scalar type.
	 * \return 
	 */
    CUDAD_CALL static Type input(const T& value, int k, const T& deriv = 1)
	{
        Type t;
        t.value_ = value;
        if (k>=0) t.derivatives_[k] = deriv;
        return t;
	}

    /**
     * \brief Creates a variable for the k-th input with the specified value.
     * Derivatives for this input are tracked.
     * \tparam k the index of the variable, 0<=k<N
     * \param value the input variable
     * \param deriv the initial value for the derivative. Usually just 1 (the default),
     *   unless you use a special scalar type.
     * \return
     */
    template<int k>
    CUDAD_CALL static Type input(const T& value, const T& deriv = 1)
    {
        static_assert(k >= 0, "0 <= k < N violated");
        static_assert(k < N, "0 <= k < N violated");
        Type t;
        t.value_ = value;
        t.derivatives_[k] = deriv;
        return t;
    }

    /**
     * \brief Constructs a variable for the constant with the specified value.
     * No derivatives are tracked for this value.
     * \param value 
     * \return 
     */
    CUDAD_CALL static Type constant(T value)
    {
        Type t;
        t.value_ = value;
        return t;
    }

    /**
     * \brief Returns the value of this forward variable.
     */
    CUDAD_CALL const T& value() const { return value_; }

    CUDAD_CALL explicit operator T() const { return value_; }

    CUDAD_CALL const TDiff* derivatives() const { return derivatives_.data(); }
    CUDAD_CALL const TDiff& derivative(int k) const
    {
        assert(k >= 0);
        assert(k < N);
        return derivatives_[k];
    }
    template<int k>
    CUDAD_CALL const TDiff& derivative() const
    {
        static_assert(k >= 0, "0 <= k < N violated");
        static_assert(k < N, "0 <= k < N violated");
        return derivatives_[k];
    }

#ifndef CUDA_NO_HOST
    /**
     * \brief Prints the variable in the form <code>value (derivative 1, ..., derivative n)</code>
     */
    friend std::ostream& operator<<(std::ostream& o, const fvar& var)
    {
        o << var.value() << " (";
        for (int k=0; k<NumDerivatives; ++k)
        {
            if (k > 0) o << ", ";
            o << var.derivative(k);
        }
        o << ")";
        return o;
    }
#endif

    //-------------------------------------------
    // OPERATOR OVERLOADING
    //-------------------------------------------
    
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL friend fvar operator+(const fvar& l, const S& r)
    {
        fvar ret = l;
        ret += r;
        return ret;
    }
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL friend fvar operator+(const S& l, const fvar& r)
    {
        return fvar(l + r.value_, r.derivatives_);
    }
    CUDAD_CALL friend fvar operator+(const fvar& l, const fvar& r)
    {
        fvar ret = l;
        ret += r;
        return ret;
    }
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar& operator+=(const S& r)
	{
        value_ += r;
        return *this;
	}
    CUDAD_CALL fvar& operator+=(const fvar& r)
    {
        value_ += r.value_;
        for (int k = 0; k < N; ++k) derivatives_[k] += r.derivatives_[k];
        return *this;
    }

    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL friend fvar operator-(const fvar& l, const S& r)
    {
        fvar ret = l;
        ret -= r;
        return ret;
    }
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL friend fvar operator-(const S& l, const fvar& r)
    {
        fvar ret;
        ret.value_ = l - r.value_;
        for (int k = 0; k < N; ++k) ret.derivatives_[k] = -r.derivatives_[k];
        return ret;
    }
    CUDAD_CALL friend fvar operator-(const fvar& l, const fvar& r)
    {
        fvar ret = l;
        ret -= r;
        return ret;
    }
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar& operator-=(const S& r)
    {
        value_ -= r;
        return *this;
    }
    CUDAD_CALL fvar& operator-=(const fvar& r)
    {
        value_ -= r.value_;
        for (int k = 0; k < N; ++k) derivatives_[k] -= r.derivatives_[k];
        return *this;
    }
    CUDAD_CALL fvar operator-()
	{
        fvar ret;
        ret.value_ = -value_;
        for (int k = 0; k < N; ++k) ret.derivatives_[k] = -derivatives_[k];
        return ret;
	}

    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL friend fvar operator*(const fvar& l, const S& r)
    {
        fvar ret = l;
        ret *= r;
        return ret;
    }
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL friend fvar operator*(const S& l, const fvar& r)
    {
        fvar ret = r;
        ret *= l;
        return ret;
    }
    template <
        typename TRight, typename TDiffRight,
        typename Out = decltype(T()*TRight()),
        typename OutDiff = decltype(TDiff()*TDiffRight())>
        CUDAD_CALL friend fvar<Out, N, OutDiff> operator*(const fvar& l, const fvar<TRight, N, TDiffRight>& r)
    {
        internal::array<OutDiff, N> deriv;
        for (int k = 0; k < N; ++k)
            deriv[k] = l.derivatives_[k] * r.value() + l.value_ * r.derivatives()[k];
        return fvar<Out, N, OutDiff>(l.value_*r.value(), deriv);
    }
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar& operator*=(const S& r)
    {
        value_ *= r;
        for (int k = 0; k < N; ++k) derivatives_[k] *= r;
        return *this;
    }
    CUDAD_CALL fvar& operator*=(const fvar& r)
    {
        for (int k = 0; k < N; ++k) 
            derivatives_[k] = derivatives_[k] * r.value_ + value_ * r.derivatives_[k];
        value_ *= r.value_;
        return *this;
    }

    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL friend fvar operator/(const fvar& l, const S& r)
    {
        fvar ret = l;
        ret /= r;
        return ret;
    }
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL friend fvar operator/(const S& l, const fvar& r)
    {
        fvar ret;
        const T invSqr = T(1) / (r.value_ * r.value_);
        ret.value_ = l / r.value_;
        for (int k = 0; k < N; ++k) 
            ret.derivatives_[k] = -l * r.derivatives_[k] * invSqr;
        return ret;
    }
    CUDAD_CALL friend fvar operator/(const fvar& l, const fvar& r)
    {
        fvar ret = l;
        ret /= r;
        return ret;
    }
    template <typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar& operator/=(const S& r)
    {
        value_ /= r;
        for (int k = 0; k < N; ++k) derivatives_[k] /= r;
        return *this;
    }
    CUDAD_CALL fvar& operator/=(const fvar& r)
    {
        const T invSqr = T(1) / (r.value_ * r.value_);
        for (int k = 0; k < N; ++k)
            derivatives_[k] = invSqr * (derivatives_[k] * r.value_ - value_ * r.derivatives_[k]);
        value_ /= r.value_;
        return *this;
    }
};

//-------------------------------------------
// FURTHER BINARY OPERATIONS
//-------------------------------------------

/**
 * \brief computes pow(a,b) = a^b
 */
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
	CUDAD_CALL fvar<T, N, TDiff> pow(const fvar<T, N, TDiff>& a, const S& b)
{
    assert(b != 0);
    using std::pow; //to call std::pow or user-defined overloads
    T value = static_cast<T>(pow(a.value(), b));
    T value2 = b * static_cast<T>(pow(a.value(), b - 1));
    internal::array<TDiff, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = value2 * a.derivatives()[k];
    return fvar<T, N, TDiff>(value, std::move(derivatives));
}

/**
 * \brief computes pow(a,b) = a^b
 */
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> pow(const S& a, const fvar<T, N, TDiff>& b)
{
    assert(b.value() != 0);
    T value = std::pow(a, b.value());
    T value2 = std::log(a) * std::pow(a, b.value());
    internal::array<TDiff, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = value2 * b.derivatives()[k];
    return fvar<T, N, TDiff>(value, std::move(derivatives));
}

/**
 * \brief computes pow(a,b) = a^b
 */
template <
    typename T, int N, typename TDiff>
    CUDAD_CALL fvar<T, N, TDiff> pow(const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b)
{
    assert(b.value() != 0);
    T value = std::pow(a.value(), b.value());
    T factor1 = b.value() * std::pow(a.value(), b.value() - 1);
    T factor2 = value * std::log(a.value());
    internal::array<TDiff, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = factor1 * a.derivatives()[k] + factor2 * b.derivatives()[k];
    return fvar<T, N, TDiff>(value, std::move(derivatives));
}


//-------------------------------------------
// LOGIC FUNCTIONS
//-------------------------------------------

/**
 * \brief computes min(a,b)
 */
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> fmin(const fvar<T, N, TDiff>& a, const S& b)
{
    if (a.value() <= b)
        return a;
    else
        return fvar<T, N, TDiff>::constant(b);
}

/**
 * \brief computes min(a,b)
 */
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> fmin(const S& a, const fvar<T, N, TDiff>& b)
{
    if (a <= b.value())
        return fvar<T, N, TDiff>::constant(a);
    else
        return b;
}

/**
 * \brief computes min(a,b)
 */
template <
    typename T, int N, typename TDiff>
    CUDAD_CALL fvar<T, N, TDiff> fmin(const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b)
{
    if (a.value() <= b.value())
        return a;
    else
        return b;
}

/**
 * \brief computes max(a,b)
 */
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> fmax(const fvar<T, N, TDiff>& a, const S& b)
{
    if (a.value() >= b)
        return a;
    else
        return fvar<T, N, TDiff>::constant(b);
}

/**
 * \brief computes max(a,b)
 */
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> fmax(const S& a, const fvar<T, N, TDiff>& b)
{
    if (a >= b.value())
        return fvar<T, N, TDiff>::constant(a);
    else
        return b;
}

/**
 * \brief computes max(a,b)
 */
template <
    typename T, int N, typename TDiff>
    CUDAD_CALL fvar<T, N, TDiff> fmax(const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b)
{
    if (a.value() >= b.value())
        return a;
    else
        return b;
}

//-------------------------------------------
// MISC UNARY FUNCTIONS
//-------------------------------------------

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> abs(const fvar<T, N, TDiff>& v)
{
    T factor = v.value() >= 0 ? T(1) : T(-1);
    return factor * v;
}
template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> fabs(const fvar<T, N, TDiff>& v)
{
    T factor = v.value() >= 0 ? T(1) : T(-1);
    return factor * v;
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> square(const fvar<T, N, TDiff>& v)
{
    return v * v;
}

//-------------------------------------------
// UNARY POWER FUNCTIONS (for a lack of a better name)
//-------------------------------------------

/**
 * \brief Helper method for unary expressions
 * where the new value is given by 'newValue' and the derivatives
 * are given by 'diffFactor * v.derivatives()'
 */
template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> _linear_unary_expr(const fvar<T, N, TDiff>& v, const T& newValue, const T& diffFactor)
{
    internal::array<TDiff, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = diffFactor * v.derivatives()[k];
    return fvar<T, N, TDiff>(newValue, std::move(derivatives));
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> exp(const fvar<T, N, TDiff>& v)
{
    using std::exp;
    T value = exp(v.value());
    return _linear_unary_expr(v, value, value);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> log(const fvar<T, N, TDiff>& v)
{
    using std::log;
    T value = log(v.value());
    T factor = T(1) / v.value();
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> log2(const fvar<T, N, TDiff>& v)
{
    using std::log2;
    T value = log2(v.value());
    T factor = T(1) / (std::log(T(2)) * v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> log10(const fvar<T, N, TDiff>& v)
{
    using std::log10;
    T value = log10(v.value());
    T factor = T(1) / (std::log(T(10)) * v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> sqrt(const fvar<T, N, TDiff>& v)
{
    using std::sqrt, std::fmax;
    T value = sqrt(v.value());
    T factor = T(1) / (2*fmax(T(1e-8), value));
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> rsqrt(const fvar<T, N, TDiff>& v)
{
    using std::sqrt, std::fmax;
    T value = T(1) / fmax(T(1e-8), sqrt(v.value()));
    T factor = T(-1) / (2 * value*value*value);
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> cbrt(const fvar<T, N, TDiff>& v)
{
    using std::cbrt, std::fmax;
    T value = cbrt(v.value());
    T factor = T(1) / fmax(T(1e-8), (3 * value * value));
    return _linear_unary_expr(v, value, factor);
}

//-------------------------------------------
// TRIGONOMETRIC FUNCTIONS
//-------------------------------------------

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> sin(const fvar<T, N, TDiff>& v)
{
    using std::sin, std::cos;
    T value = sin(v.value());
    T factor = cos(v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> cos(const fvar<T, N, TDiff>& v)
{
    using std::sin, std::cos;
    T value = cos(v.value());
    T factor = -sin(v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> tan(const fvar<T, N, TDiff>& v)
{
    using std::tan, std::cos;
    T value = tan(v.value());
    T valCos = cos(v.value());
    T factor = T(1) / (valCos * valCos);
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> asin(const fvar<T, N, TDiff>& v)
{
    using std::asin, std::sqrt;
    T value = asin(v.value());
    T factor = T(1) / sqrt(1 - v.value() * v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> acos(const fvar<T, N, TDiff>& v)
{
    using std::acos, std::sqrt;
    T value = acos(v.value());
    T factor = T(-1) / sqrt(1 - v.value() * v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> atan(const fvar<T, N, TDiff>& v)
{
    using std::atan;
    T value = atan(v.value());
    T factor = T(1) / (1 + v.value()*v.value());
    return _linear_unary_expr(v, value, factor);
}

//-------------------------------------------
// HYBERBOLIC FUNCTIONS
//-------------------------------------------

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> sinh(const fvar<T, N, TDiff>& v)
{
    using std::sinh, std::cosh;
    T value = sinh(v.value());
    T factor = cosh(v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> cosh(const fvar<T, N, TDiff>& v)
{
    using std::sinh, std::cosh;
    T value = cosh(v.value());
    T factor = sinh(v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> tanh(const fvar<T, N, TDiff>& v)
{
    using std::tanh, std::cosh;
    T value = tanh(v.value());
    T valCosh = cosh(v.value());
    T factor = T(1) / (valCosh * valCosh);
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> asinh(const fvar<T, N, TDiff>& v)
{
    using std::asinh, std::sqrt;
    T value = asinh(v.value());
    T factor = T(1) / sqrt(1 + v.value() * v.value());
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> acosh(const fvar<T, N, TDiff>& v)
{
    using std::acosh, std::sqrt;
    T value = acosh(v.value());
    T factor = T(1) / (sqrt(v.value() - 1) * sqrt(v.value() + 1));
    return _linear_unary_expr(v, value, factor);
}

template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> atanh(const fvar<T, N, TDiff>& v)
{
    using std::atanh;
    T value = atanh(v.value());
    T factor = T(1) / (1 - v.value() * v.value());
    return _linear_unary_expr(v, value, factor);
}

//-------------------------------------------
// LERP + CLAMP
//-------------------------------------------

//Lerp between a and b: returns a iff t=0, b iff t=1, linear in between
template <typename T, int N, typename TDiff>
CUDAD_CALL fvar<T, N, TDiff> lerp(const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b, const fvar<T, N, TDiff>& t)
{
    return a + t * (b - a);
}
//Lerp between a and b: returns a iff t=0, b iff t=1, linear in between
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
CUDAD_CALL fvar<T, N, TDiff> lerp(const S& a, const S& b, const fvar<T, N, TDiff>& t)
{
    return a + t * (b - a);
}
//Lerp between a and b: returns a iff t=0, b iff t=1, linear in between
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> lerp(const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b, const S& t)
{
    return a + t * (b - a);
}

template <
    typename T, int N, typename TDiff>
    CUDAD_CALL fvar<T, N, TDiff> clamp(const fvar<T, N, TDiff>& v, const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b)
{
    if (v.value() <= a.value())
        return a;
    else if (b.value() <= v.value())
        return b;
    else
        return v;
}

template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> clamp(const fvar<T, N, TDiff>& v, const S& a, const S& b)
{
    if (v.value() <= a)
        return fvar<T, N, TDiff>::constant(a);
    else if (b <= v.value())
        return fvar<T, N, TDiff>::constant(b);
    else
        return v;
}

template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> clamp(const S& v, const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b)
{
    if (v <= a.value())
        return a;
    else if (b.value() <= v)
        return b;
    else
        return fvar<T, N, TDiff>::constant(v);
}

CUDAD_NAMESPACE_END

#endif

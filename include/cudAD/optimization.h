#pragma once
#ifndef __CUDAD_OPTIMIZATION_H__
#define __CUDAD_OPTIMIZATION_H__

#ifndef CUDA_NO_HOST

#include <cassert>
#include <valarray>
#include <functional>

#include "macros.h"
CUDAD_NAMESPACE_BEGIN

/**
 * \brief Base class for the different optimizers
 * \tparam Scalar 
 */
template <class Scalar>
class Optimizer
{
public:
    typedef Scalar Scalar_t;

    typedef std::valarray<Scalar> Vector_t;
    /**
     * \brief the scalar function to minimize.
     * It has the form of
     * <code>Scalar function(const std::valarray<Scalar>& parameters, std::valarray<Scalar>& out_derivatives)</code>
     * and returns the scalar value.
     */
    typedef std::function<Scalar(const Vector_t&, Vector_t&)> Fun_t;

protected:
    const int n_; //number of variables

    const Fun_t fun_;
    Vector_t nextX_;
    Vector_t currentX_;
    Vector_t currentGrad_;
    Scalar_t currentValue_;

    int k_; //iteration counter
    int maxIterations_;
    Scalar_t minGradientNorm_;

    /**
     * \brief Initializes the optimizer.
     * \param fun the function to minimize
     * \param n the number of variables
     */
    Optimizer(const Fun_t& fun, int n)
        : fun_(fun), n_(n), k_(0), maxIterations_(-1), minGradientNorm_(-1)
    {}

public:
    virtual ~Optimizer() {}

    int maxIterations() const { return maxIterations_; }
    /**
     * \brief Sets the maximal number of iterations until termination.
     * Use \c -1 if it should be unbounded.
     */
    void setMaxIterations(int maxIterations) { maxIterations_ = maxIterations; }

    Scalar_t minGradientNorm() const { return minGradientNorm_; }
    /**
     * \brief Sets the minimal gradient norm.
     * If the gradient norm falls below this value, the optimization is terminated.
     * Use \c -1 if it should be unbounded.
     */
    void setMinGradientNorm(Scalar_t minGradientNorm) { minGradientNorm_ = minGradientNorm; }

    /**
     * \brief Returns the current iteration index
     */
    int currentIteration() const { return k_; }

    /**
     * \brief Returns the value of the current step.
     * These are the values for which gradients were computed
     * \see currentValue()
     * \see currentGrad()
     */
    const Vector_t& currentX() const { return currentX_; }
    /**
     * \brief Returns the value of the current step.
     * These are the values for which gradients were computed
     * \see currentValue()
     * \see currentX()
     */
    const Vector_t& currentGrad() const { return currentGrad_; }
    /**
     * \brief Returns the value of the current step.
     * These are the values for which gradients were computed
     * \see currentValue()
     * \see currentGrad()
     */
    const Scalar_t& currentValue() const { return currentValue_; }
    /**
     * \brief Returns the value of the next evaluation position.
     */
    const Vector_t& nextX() const { return nextX_; }

    /**
     * \brief Initializes the optimizer with the start value 'initial'.
     * \param initial the initial values for the variables
     */
    virtual void init(const Vector_t& initial) {
        assert(n_ == initial.size());
        k_ = 0;
        nextX_ = initial;
        currentGrad_ = Vector_t(Scalar_t(0), initial.size());
    }

    /**
     * \brief Performs a single step of the optimization.
     * It performs the following steps:
     * 1. copy \c nextX to \c currentX
     * 2. evaluate the function on \c currentX -> \c currentValue and \c currentGrad
     * 3. compute the next evaluation position -> \c nextX
     * 4. increase the iteration counter
     */
    virtual void step() = 0;

    /**
     * \brief Checks for termination.
     * Termination happens if
     * - the number of iterations exceeds \ref maxIterations()
     * - the gradient norm falls below \ref minGradientNorm()
     * \return true iff at least one termination criterion is fulfilled
     */
    virtual bool checkTermination()
    {
        if (maxIterations_ >= 0 && k_ > maxIterations_) return true;
        if (minGradientNorm_ >= 0)
        {
            const auto norm = (currentGrad() * currentGrad()).sum();
            if (norm < minGradientNorm_ * minGradientNorm_) return true;
        }
        return false;
    }

    virtual Vector_t solve(const Vector_t& initial)
    {
        init(initial);
        do
        {
            step();
        } while (!checkTermination());
        return currentX();
    }

};

template <class Scalar>
class GradientDescent : public Optimizer<Scalar>
{
public:
    using typename Optimizer<Scalar>::Scalar_t;
    using typename Optimizer<Scalar>::Vector_t;
    using typename Optimizer<Scalar>::Fun_t;

private:
    Scalar_t initialStepsize_;

public:
    GradientDescent(const typename Optimizer<Scalar>::Fun_t& fun, int n, Scalar_t initialStepsize = 1e-2)
        : Optimizer<Scalar>(fun, n), initialStepsize_(initialStepsize)
    {}

    Scalar_t initialStepsize() const { return initialStepsize_; }
    void setInitialStepsize(Scalar_t stepsize)
    {
        assert(stepsize > 0);
        initialStepsize_ = stepsize;
    }

    void step() override
    {
        //save current values
        Vector_t previousX = this->currentX_;
        Vector_t previousGrad = this->currentGrad_;

        //copy next to current
        this->currentX_ = this->nextX_;

        //evaluate
        this->currentValue_ = this->fun_(this->currentX_, this->currentGrad_);

        //compute next step
        Scalar_t gamma;
        if (this->k_ == 0)
        {
            gamma = initialStepsize_;
        } else
        {
            //Barzilai-Borwein
            Vector_t xDiff = this->currentX_ - previousX;
            Vector_t gradDiff = this->currentGrad_ - previousGrad;
            Scalar_t nom = (xDiff * gradDiff).sum();
            Scalar_t denom = (gradDiff * gradDiff).sum();
            gamma = std::fabs(nom) / denom;
        }
        this->nextX_ = this->currentX_ - gamma * this->currentGrad_;

        //increase the iteration counter
        ++this->k_;
    }
};

CUDAD_NAMESPACE_END

#endif
#endif

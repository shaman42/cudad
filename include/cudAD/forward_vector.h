#pragma once
#ifndef __CUDAD_FORWARD_VECTOR_H__
#define __CUDAD_FORWARD_VECTOR_H__

#if __has_include(<helper_math.cuh>)
#include <helper_math.cuh>
#define __CUDAD_FORWARD_VECTOR_AVAILABLE 1
#elif __has_include("helper_math.cuh")
#include "helper_math.cuh"
#define __CUDAD_FORWARD_VECTOR_AVAILABLE 1
#else
#warning "No helper_math.cuh found in include path, include it yourself before this file"
#endif

#ifdef __CUDAD_FORWARD_VECTOR_AVAILABLE
#include "forward.h"

/**
 * Overloads of the functions in helper_math.cuh
 * for forward variables
 */

CUDAD_NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////////////////
// CONSTRUCTORS for inputs
////////////////////////////////////////////////////////////////////////////////

template<int N>
CUDAD_CALL fvar<float2, N> make_float2in(const float2& xy, const int2& indices)
{
    internal::array<float2, N> derivatives;
    if (indices.x>=0) derivatives[indices.x] = ::make_float2(1, 0);
    if (indices.y>=0) derivatives[indices.y] = ::make_float2(0, 1);
    return fvar<float3, N>{xy, derivatives};
}

template<int N, int X = 0, int Y = 1>
CUDAD_CALL fvar<float2, N> make_float2in(const float2& xy)
{
    static_assert(X != Y, "indices for for the derivatives of x,y must be distinct.");
    static_assert(0 <= X && X < N, "derivative index is out of bounds");
    static_assert(0 <= Y && Y < N, "derivative index is out of bounds");

    internal::array<float2, N> derivatives;
    derivatives[X] = ::make_float2(1, 0);
    derivatives[Y] = ::make_float2(0, 1);
    return fvar<float3, N>{xy, derivatives};
}

template<int N, int X = 0, int Y = 1>
CUDAD_CALL fvar<float2, N> make_float2in(float x, float y)
{
    return make_float2in<N, X, Y>(::make_float2(x, y));
}

template<int N>
CUDAD_CALL fvar<float3, N> make_float3in(const float3& xyz, const int3& indices)
{
    internal::array<float3, N> derivatives;
    if (indices.x>=0) derivatives[indices.x] = ::make_float3(1, 0, 0);
    if (indices.y>=0) derivatives[indices.y] = ::make_float3(0, 1, 0);
    if (indices.z>=0) derivatives[indices.z] = ::make_float3(0, 0, 1);
    return fvar<float3, N>{xyz, derivatives};
}

template<int N, int X = 0, int Y = 1, int Z = 2>
CUDAD_CALL fvar<float3, N> make_float3in(const float3& xyz)
{
    static_assert(X != Y && X != Z && Y != Z, 
        "indices for for the derivatives of x,y,z must be distinct.");
    static_assert(0 <= X && X < N, "derivative index is out of bounds");
    static_assert(0 <= Y && Y < N, "derivative index is out of bounds");
    static_assert(0 <= Z && Z < N, "derivative index is out of bounds");

    internal::array<float3, N> derivatives;
    derivatives[X] = ::make_float3(1, 0, 0);
    derivatives[Y] = ::make_float3(0, 1, 0);
    derivatives[Z] = ::make_float3(0, 0, 1);
    return fvar<float3, N>{xyz, derivatives};
}

template<int N, int X = 0, int Y = 1, int Z = 2>
CUDAD_CALL fvar<float3, N> make_float3in(float x, float y, float z)
{
    return make_float3in<N,X,Y,Z>(::make_float3(x, y, z));
}

template<int N>
CUDAD_CALL fvar<float4, N> make_float4in(const float4& xyzw, const int4& indices)
{
    internal::array<float4, N> derivatives;
    if (indices.x>=0) derivatives[indices.x] = ::make_float4(1, 0, 0, 0);
    if (indices.y>=0) derivatives[indices.y] = ::make_float4(0, 1, 0, 0);
    if (indices.z>=0) derivatives[indices.z] = ::make_float4(0, 0, 1, 0);
    if (indices.w>=0) derivatives[indices.w] = ::make_float4(0, 0, 0, 1);
    return fvar<float4, N>{xyzw, derivatives};
}

template<int N, int X = 0, int Y = 1, int Z = 2, int W = 3>
CUDAD_CALL fvar<float4, N> make_float4in(const float4& xyzw)
{
    static_assert(X != Y && X != Z && X!=W && Y != Z && Y != W && Z != W,
        "indices for for the derivatives of x,y,z,w must be distinct.");
    static_assert(0 <= X && X < N, "derivative index is out of bounds");
    static_assert(0 <= Y && Y < N, "derivative index is out of bounds");
    static_assert(0 <= Z && Z < N, "derivative index is out of bounds");
    static_assert(0 <= W && W < N, "derivative index is out of bounds");

    internal::array<float4, N> derivatives;
    derivatives[X] = ::make_float4(1, 0, 0, 0);
    derivatives[Y] = ::make_float4(0, 1, 0, 0);
    derivatives[Z] = ::make_float4(0, 0, 1, 0);
    derivatives[W] = ::make_float4(0, 0, 0, 1);
    return fvar<float4, N>{xyzw, derivatives};
}

template<int N, int X = 0, int Y = 1, int Z = 2, int W = 3>
CUDAD_CALL fvar<float4, N> make_float4in(float x, float y, float z, float w)
{
    return make_float4in<N,X,Y,Z>(::make_float4(x, y, z, w));
}


template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<float2, N> make_float2(
    const fvar<float, N>& x, const fvar<float, N>& y)
{
    internal::array<float2, N> derivatives;
    for (int k = 0; k < N; ++k) 
        derivatives[k] = ::make_float2(x.derivatives()[k], y.derivatives()[k]);
    return fvar<float2, N>(::make_float2(x.value(), y.value()), derivatives);
}

template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<float3, N> make_float3(
    const fvar<float, N>& x, const fvar<float, N>& y, const fvar<float, N>& z)
{
    internal::array<float3, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = ::make_float3(x.derivatives()[k], y.derivatives()[k], z.derivatives()[k]);
    return fvar<float3, N>(::make_float3(x.value(), y.value(), z.value()), derivatives);
}

template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<float4, N> make_float4(
    const fvar<float, N>& x, const fvar<float, N>& y, 
    const fvar<float, N>& z, const fvar<float, N>& w)
{
    internal::array<float4, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = ::make_float4(
            x.derivatives()[k], y.derivatives()[k],
            z.derivatives()[k], w.derivatives()[k]);
    return fvar<float4, N>(::make_float4(x.value(), y.value(), z.value(), w.value()), derivatives);
}




template<int N>
CUDAD_CALL fvar<double2, N> make_double2in(const double2& xy, const int2& indices)
{
    internal::array<double2, N> derivatives;
    if (indices.x >= 0) derivatives[indices.x] = ::make_double2(1, 0);
    if (indices.y >= 0) derivatives[indices.y] = ::make_double2(0, 1);
    return fvar<double3, N>{xy, derivatives};
}

template<int N, int X = 0, int Y = 1>
CUDAD_CALL fvar<double2, N> make_double2in(const double2& xy)
{
    static_assert(X != Y, "indices for for the derivatives of x,y must be distinct.");
    static_assert(0 <= X && X < N, "derivative index is out of bounds");
    static_assert(0 <= Y && Y < N, "derivative index is out of bounds");

    internal::array<double2, N> derivatives;
    derivatives[X] = ::make_double2(1, 0);
    derivatives[Y] = ::make_double2(0, 1);
    return fvar<double3, N>{xy, derivatives};
}

template<int N, int X = 0, int Y = 1>
CUDAD_CALL fvar<double2, N> make_double2in(double x, double y)
{
    return make_double2in<N, X, Y>(::make_double2(x, y));
}

template<int N>
CUDAD_CALL fvar<double3, N> make_double3in(const double3& xyz, const int3& indices)
{
    internal::array<double3, N> derivatives;
    if (indices.x >= 0) derivatives[indices.x] = ::make_double3(1, 0, 0);
    if (indices.y >= 0) derivatives[indices.y] = ::make_double3(0, 1, 0);
    if (indices.z >= 0) derivatives[indices.z] = ::make_double3(0, 0, 1);
    return fvar<double3, N>{xyz, derivatives};
}

template<int N, int X = 0, int Y = 1, int Z = 2>
CUDAD_CALL fvar<double3, N> make_double3in(const double3& xyz)
{
    static_assert(X != Y && X != Z && Y != Z,
        "indices for for the derivatives of x,y,z must be distinct.");
    static_assert(0 <= X && X < N, "derivative index is out of bounds");
    static_assert(0 <= Y && Y < N, "derivative index is out of bounds");
    static_assert(0 <= Z && Z < N, "derivative index is out of bounds");

    internal::array<double3, N> derivatives;
    derivatives[X] = ::make_double3(1, 0, 0);
    derivatives[Y] = ::make_double3(0, 1, 0);
    derivatives[Z] = ::make_double3(0, 0, 1);
    return fvar<double3, N>{xyz, derivatives};
}

template<int N, int X = 0, int Y = 1, int Z = 2>
CUDAD_CALL fvar<double3, N> make_double3in(double x, double y, double z)
{
    return make_double3in<N, X, Y, Z>(::make_double3(x, y, z));
}

template<int N>
CUDAD_CALL fvar<double4, N> make_double4in(const double4& xyzw, const int4& indices)
{
    internal::array<double4, N> derivatives;
    if (indices.x >= 0) derivatives[indices.x] = ::make_double4(1, 0, 0, 0);
    if (indices.y >= 0) derivatives[indices.y] = ::make_double4(0, 1, 0, 0);
    if (indices.z >= 0) derivatives[indices.z] = ::make_double4(0, 0, 1, 0);
    if (indices.w >= 0) derivatives[indices.w] = ::make_double4(0, 0, 0, 1);
    return fvar<double4, N>{xyzw, derivatives};
}

template<int N, int X = 0, int Y = 1, int Z = 2, int W = 3>
CUDAD_CALL fvar<double4, N> make_double4in(const double4& xyzw)
{
    static_assert(X != Y && X != Z && X != W && Y != Z && Y != W && Z != W,
        "indices for for the derivatives of x,y,z,w must be distinct.");
    static_assert(0 <= X && X < N, "derivative index is out of bounds");
    static_assert(0 <= Y && Y < N, "derivative index is out of bounds");
    static_assert(0 <= Z && Z < N, "derivative index is out of bounds");
    static_assert(0 <= W && W < N, "derivative index is out of bounds");

    internal::array<double4, N> derivatives;
    derivatives[X] = ::make_double4(1, 0, 0, 0);
    derivatives[Y] = ::make_double4(0, 1, 0, 0);
    derivatives[Z] = ::make_double4(0, 0, 1, 0);
    derivatives[W] = ::make_double4(0, 0, 0, 1);
    return fvar<double4, N>{xyzw, derivatives};
}

template<int N, int X = 0, int Y = 1, int Z = 2, int W = 3>
CUDAD_CALL fvar<double4, N> make_double4in(double x, double y, double z, double w)
{
    return make_double4in<N, X, Y, Z>(::make_double4(x, y, z, w));
}


template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<double2, N> make_double2(
    const fvar<double, N>& x, const fvar<double, N>& y)
{
    internal::array<double2, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = ::make_double2(x.derivatives()[k], y.derivatives()[k]);
    return fvar<double2, N>(::make_double2(x.value(), y.value()), derivatives);
}

template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<double3, N> make_double3(
    const fvar<double, N>& x, const fvar<double, N>& y, const fvar<double, N>& z)
{
    internal::array<double3, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = ::make_double3(x.derivatives()[k], y.derivatives()[k], z.derivatives()[k]);
    return fvar<double3, N>(::make_double3(x.value(), y.value(), z.value()), derivatives);
}

template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<double4, N> make_double4(
    const fvar<double, N>& x, const fvar<double, N>& y,
    const fvar<double, N>& z, const fvar<double, N>& w)
{
    internal::array<double4, N> derivatives;
    for (int k = 0; k < N; ++k)
        derivatives[k] = ::make_double4(
            x.derivatives()[k], y.derivatives()[k],
            z.derivatives()[k], w.derivatives()[k]);
    return fvar<double4, N>(::make_double4(x.value(), y.value(), z.value(), w.value()), derivatives);
}




////////////////////////////////////////////////////////////////////////////////
// coefficient getter
////////////////////////////////////////////////////////////////////////////////

CUDAD_CALL float getX(const float2& a)
{
    return a.x;
}
CUDAD_CALL float getY(const float2& a)
{
    return a.y;
}
CUDAD_CALL float getX(const float3& a)
{
    return a.x;
}
CUDAD_CALL float getY(const float3& a)
{
    return a.y;
}
CUDAD_CALL float getZ(const float3& a)
{
    return a.z;
}
CUDAD_CALL float getX(const float4& a)
{
    return a.x;
}
CUDAD_CALL float getY(const float4& a)
{
    return a.y;
}
CUDAD_CALL float getZ(const float4& a)
{
    return a.z;
}
CUDAD_CALL float getW(const float4& a)
{
    return a.w;
}


template<int N>
CUDAD_CALL fvar<float, N> getX(const fvar<float2, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].x;
    return fvar<float, N>(a.value().x, derivatives);
}
template<int N>
CUDAD_CALL fvar<float, N> getY(const fvar<float2, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].y;
    return fvar<float, N>(a.value().y, derivatives);
}

template<int N>
CUDAD_CALL fvar<float, N> getX(const fvar<float3, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].x;
    return fvar<float, N>(a.value().x, derivatives);
}
template<int N>
CUDAD_CALL fvar<float, N> getY(const fvar<float3, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].y;
    return fvar<float, N>(a.value().y, derivatives);
}
template<int N>
CUDAD_CALL fvar<float, N> getZ(const fvar<float3, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].z;
    return fvar<float, N>(a.value().z, derivatives);
}

template<int N>
CUDAD_CALL fvar<float, N> getX(const fvar<float4, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].x;
    return fvar<float, N>(a.value().x, derivatives);
}
template<int N>
CUDAD_CALL fvar<float, N> getY(const fvar<float4, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].y;
    return fvar<float, N>(a.value().y, derivatives);
}
template<int N>
CUDAD_CALL fvar<float, N> getZ(const fvar<float4, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].z;
    return fvar<float, N>(a.value().z, derivatives);
}
template<int N>
CUDAD_CALL fvar<float, N> getW(const fvar<float4, N>& a)
{
    internal::array<float, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].w;
    return fvar<float, N>(a.value().w, derivatives);
}




CUDAD_CALL double getX(const double2& a)
{
    return a.x;
}
CUDAD_CALL double getY(const double2& a)
{
    return a.y;
}
CUDAD_CALL double getX(const double3& a)
{
    return a.x;
}
CUDAD_CALL double getY(const double3& a)
{
    return a.y;
}
CUDAD_CALL double getZ(const double3& a)
{
    return a.z;
}
CUDAD_CALL double getX(const double4& a)
{
    return a.x;
}
CUDAD_CALL double getY(const double4& a)
{
    return a.y;
}
CUDAD_CALL double getZ(const double4& a)
{
    return a.z;
}
CUDAD_CALL double getW(const double4& a)
{
    return a.w;
}


template<int N>
CUDAD_CALL fvar<double, N> getX(const fvar<double2, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].x;
    return fvar<double, N>(a.value().x, derivatives);
}
template<int N>
CUDAD_CALL fvar<double, N> getY(const fvar<double2, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].y;
    return fvar<double, N>(a.value().y, derivatives);
}

template<int N>
CUDAD_CALL fvar<double, N> getX(const fvar<double3, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].x;
    return fvar<double, N>(a.value().x, derivatives);
}
template<int N>
CUDAD_CALL fvar<double, N> getY(const fvar<double3, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].y;
    return fvar<double, N>(a.value().y, derivatives);
}
template<int N>
CUDAD_CALL fvar<double, N> getZ(const fvar<double3, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].z;
    return fvar<double, N>(a.value().z, derivatives);
}

template<int N>
CUDAD_CALL fvar<double, N> getX(const fvar<double4, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].x;
    return fvar<double, N>(a.value().x, derivatives);
}
template<int N>
CUDAD_CALL fvar<double, N> getY(const fvar<double4, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].y;
    return fvar<double, N>(a.value().y, derivatives);
}
template<int N>
CUDAD_CALL fvar<double, N> getZ(const fvar<double4, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].z;
    return fvar<double, N>(a.value().z, derivatives);
}
template<int N>
CUDAD_CALL fvar<double, N> getW(const fvar<double4, N>& a)
{
    internal::array<double, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = a.derivatives()[k].w;
    return fvar<double, N>(a.value().w, derivatives);
}

////////////////////////////////////////////////////////////////////////////////
// broadcasting
////////////////////////////////////////////////////////////////////////////////

CUDAD_CALL_NOCONSTEXPR float2 broadcast2(float a) { return ::make_float2(a); }
template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<float2, N> broadcast2(const fvar<float, N>& a)
{
    internal::array<float2, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = ::make_float2(a.derivatives()[k]);
    return fvar<float2, N>(::make_float2(a.value()), derivatives);
}

CUDAD_CALL_NOCONSTEXPR float3 broadcast3(float a) { return ::make_float3(a); }
template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<float3, N> broadcast3(const fvar<float, N>& a)
{
    internal::array<float3, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = ::make_float3(a.derivatives()[k]);
    return fvar<float3, N>(::make_float3(a.value()), derivatives);
}

CUDAD_CALL_NOCONSTEXPR float4 broadcast4(float a) { return ::make_float4(a); }
template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<float4, N> broadcast4(const fvar<float, N>& a)
{
    internal::array<float4, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = ::make_float4(a.derivatives()[k]);
    return fvar<float4, N>(::make_float4(a.value()), derivatives);
}




CUDAD_CALL_NOCONSTEXPR double2 broadcast2(double a) { return ::make_double2(a, a); }
template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<double2, N> broadcast2(const fvar<double, N>& a)
{
    internal::array<double2, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = ::make_double2(a.derivatives()[k]);
    return fvar<double2, N>(::make_double2(a.value()), derivatives);
}

CUDAD_CALL_NOCONSTEXPR double3 broadcast3(double a) { return ::make_double3(a, a, a); }
template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<double3, N> broadcast3(const fvar<double, N>& a)
{
    internal::array<double3, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = ::make_double3(a.derivatives()[k]);
    return fvar<double3, N>(::make_double3(a.value()), derivatives);
}

CUDAD_CALL_NOCONSTEXPR double4 broadcast4(double a) { return ::make_double4(a, a, a, a); }
template<int N>
CUDAD_CALL_NOCONSTEXPR fvar<double4, N> broadcast4(const fvar<double, N>& a)
{
    internal::array<double4, N> derivatives;
    for (int k = 0; k < N; ++k) derivatives[k] = ::make_double4(a.derivatives()[k]);
    return fvar<double4, N>(::make_double4(a.value()), derivatives);
}



////////////////////////////////////////////////////////////////////////////////
// dot product
////////////////////////////////////////////////////////////////////////////////

template<int N>
CUDAD_CALL fvar<float, N> dot(const fvar<float2, N>& a, const fvar<float2, N>& b)
{
    return getX(a) * getX(b) + getY(a) * getY(b);
}
template<int N>
CUDAD_CALL fvar<float, N> dot(const float2& a, const fvar<float2, N>& b)
{
    return a.x * getX(b) + a.y * getY(b);
}
template<int N>
CUDAD_CALL fvar<float, N> dot(const fvar<float2, N>& a, const float2& b)
{
    return getX(a) * b.x + getY(a) * b.y;
}

template<int N>
CUDAD_CALL fvar<float, N> dot(const fvar<float3, N>& a, const fvar<float3, N>& b)
{
    return getX(a) * getX(b) + getY(a) * getY(b) + getZ(a) * getZ(b);
}
template<int N>
CUDAD_CALL fvar<float, N> dot(const float3& a, const fvar<float3, N>& b)
{
    return a.x * getX(b) + a.y * getY(b) + a.z * getZ(b);
}
template<int N>
CUDAD_CALL fvar<float, N> dot(const fvar<float3, N>& a, const float3& b)
{
    return getX(a) * b.x + getY(a) * b.y + getZ(a) * b.z;
}

template<int N>
CUDAD_CALL fvar<float, N> dot(const fvar<float4, N>& a, const fvar<float4, N>& b)
{
    return getX(a) * getX(b) + getY(a) * getY(b) + getZ(a) * getZ(b) + getW(a) * getW(b);
}
template<int N>
CUDAD_CALL fvar<float, N> dot(const float4& a, const fvar<float4, N>& b)
{
    return a.x * getX(b) + a.y * getY(b) + a.z * getZ(b) + a.w * getW(b);
}
template<int N>
CUDAD_CALL fvar<float, N> dot(const fvar<float4, N>& a, const float4& b)
{
    return getX(a) * b.x + getY(a) * b.y + getZ(a) * b.z + getW(a) * b.w;
}



template<int N>
CUDAD_CALL fvar<double, N> dot(const fvar<double2, N>& a, const fvar<double2, N>& b)
{
    return getX(a) * getX(b) + getY(a) * getY(b);
}
template<int N>
CUDAD_CALL fvar<double, N> dot(const double2& a, const fvar<double2, N>& b)
{
    return a.x * getX(b) + a.y * getY(b);
}
template<int N>
CUDAD_CALL fvar<double, N> dot(const fvar<double2, N>& a, const double2& b)
{
    return getX(a) * b.x + getY(a) * b.y;
}

template<int N>
CUDAD_CALL fvar<double, N> dot(const fvar<double3, N>& a, const fvar<double3, N>& b)
{
    return getX(a) * getX(b) + getY(a) * getY(b) + getZ(a) * getZ(b);
}
template<int N>
CUDAD_CALL fvar<double, N> dot(const double3& a, const fvar<double3, N>& b)
{
    return a.x * getX(b) + a.y * getY(b) + a.z * getZ(b);
}
template<int N>
CUDAD_CALL fvar<double, N> dot(const fvar<double3, N>& a, const double3& b)
{
    return getX(a) * b.x + getY(a) * b.y + getZ(a) * b.z;
}

template<int N>
CUDAD_CALL fvar<double, N> dot(const fvar<double4, N>& a, const fvar<double4, N>& b)
{
    return getX(a) * getX(b) + getY(a) * getY(b) + getZ(a) * getZ(b) + getW(a) * getW(b);
}
template<int N>
CUDAD_CALL fvar<double, N> dot(const double4& a, const fvar<double4, N>& b)
{
    return a.x * getX(b) + a.y * getY(b) + a.z * getZ(b) + a.w * getW(b);
}
template<int N>
CUDAD_CALL fvar<double, N> dot(const fvar<double4, N>& a, const double4& b)
{
    return getX(a) * b.x + getY(a) * b.y + getZ(a) * b.z + getW(a) * b.w;
}

////////////////////////////////////////////////////////////////////////////////
// length
////////////////////////////////////////////////////////////////////////////////

template<int N>
CUDAD_CALL fvar<float, N> lengthSquared(const fvar<float2, N>& v)
{
    return dot(v, v);
}
template<int N>
CUDAD_CALL fvar<float, N> lengthSquared(const fvar<float3, N>& v)
{
    return dot(v, v);
}
template<int N>
CUDAD_CALL fvar<float, N> lengthSquared(const fvar<float4, N>& v)
{
    return dot(v, v);
}

template<int N>
CUDAD_CALL fvar<float, N> length(const fvar<float2, N>& v)
{
    return sqrt(dot(v, v));
}
template<int N>
CUDAD_CALL fvar<float, N> length(const fvar<float3, N>& v)
{
    return sqrt(dot(v, v));
}
template<int N>
CUDAD_CALL fvar<float, N> length(const fvar<float4, N>& v)
{
    return sqrt(dot(v, v));
}

template<int N>
CUDAD_CALL fvar<float2, N> normalize(const fvar<float2, N>& v)
{
    fvar<float, N> invLen = rsqrt(dot(v, v));
    return invLen * v;
}
template<int N>
CUDAD_CALL fvar<float3, N> normalize(const fvar<float3, N>& v)
{
    fvar<float, N> invLen = rsqrt(dot(v, v));
    return invLen * v;
}
template<int N>
CUDAD_CALL fvar<float4, N> normalize(const fvar<float4, N>& v)
{
    fvar<float, N> invLen = rsqrt(dot(v, v));
    return invLen * v;
}

template<int N>
CUDAD_CALL fvar<float3, N> clamp(const fvar<float3, N>& v, float a, float b)
{
    float3 rv = clamp(v.value(), a, b);
    float3 f = make_float3(
        v.value().x <= a || v.value().x >= b,
        v.value().y <= a || v.value().y >= b,
        v.value().z <= a || v.value().z >= b);
    internal::array<float3, N> deriv;
    for (int k = 0; k < N; ++k)
        deriv[k] = v.derivatives_[k] * f;
    return fvar<float3, N>(rv, deriv);
}
template<int N>
CUDAD_CALL float3 clamp(const fvar<float3, N>& v, float3 a, float3 b)
{
    float3 rv = clamp(v.value(), a, b);
    float3 f = make_float3(
        v.value().x <= a.x || v.value().x >= b.x,
        v.value().y <= a.y || v.value().y >= b.y,
        v.value().z <= a.z || v.value().z >= b.z);
    internal::array<float3, N> deriv;
    for (int k = 0; k < N; ++k)
        deriv[k] = v.derivatives_[k] * f;
    return fvar<float3, N>(rv, deriv);
}





template<int N>
CUDAD_CALL fvar<double, N> lengthSquared(const fvar<double2, N>& v)
{
    return dot(v, v);
}
template<int N>
CUDAD_CALL fvar<double, N> lengthSquared(const fvar<double3, N>& v)
{
    return dot(v, v);
}
template<int N>
CUDAD_CALL fvar<double, N> lengthSquared(const fvar<double4, N>& v)
{
    return dot(v, v);
}

template<int N>
CUDAD_CALL fvar<double, N> length(const fvar<double2, N>& v)
{
    return sqrt(dot(v, v));
}
template<int N>
CUDAD_CALL fvar<double, N> length(const fvar<double3, N>& v)
{
    return sqrt(dot(v, v));
}
template<int N>
CUDAD_CALL fvar<double, N> length(const fvar<double4, N>& v)
{
    return sqrt(dot(v, v));
}

template<int N>
CUDAD_CALL fvar<double2, N> normalize(const fvar<double2, N>& v)
{
    fvar<double, N> invLen = rsqrt(dot(v, v));
    return invLen * v;
}
template<int N>
CUDAD_CALL fvar<double3, N> normalize(const fvar<double3, N>& v)
{
    fvar<double, N> invLen = rsqrt(dot(v, v));
    return invLen * v;
}
template<int N>
CUDAD_CALL fvar<double4, N> normalize(const fvar<double4, N>& v)
{
    fvar<double, N> invLen = rsqrt(dot(v, v));
    return invLen * v;
}

template<int N>
CUDAD_CALL fvar<double3, N> clamp(const fvar<double3, N>& v, double a, double b)
{
    double3 rv = clamp(v.value(), a, b);
    double3 f = make_double3(
        v.value().x <= a || v.value().x >= b,
        v.value().y <= a || v.value().y >= b,
        v.value().z <= a || v.value().z >= b);
    internal::array<double3, N> deriv;
    for (int k = 0; k < N; ++k)
        deriv[k] = v.derivatives_[k] * f;
    return fvar<double3, N>(rv, deriv);
}
template<int N>
CUDAD_CALL double3 clamp(const fvar<double3, N>& v, double3 a, double3 b)
{
    double3 rv = clamp(v.value(), a, b);
    double3 f = make_double3(
        v.value().x <= a.x || v.value().x >= b.x,
        v.value().y <= a.y || v.value().y >= b.y,
        v.value().z <= a.z || v.value().z >= b.z);
    internal::array<double3, N> deriv;
    for (int k = 0; k < N; ++k)
        deriv[k] = v.derivatives_[k] * f;
    return fvar<double3, N>(rv, deriv);
}

CUDAD_NAMESPACE_END

#endif

#endif

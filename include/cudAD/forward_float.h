#pragma once
/*
 * Float-name aliases (e.g. sqrtf, fminf) for regular (double) functions (sqrt, fmin)
 */


#include "forward.h"

#ifndef CUDAD_CHECK_SINGLE_PRECISION_FUNCTION_TYPE
#define CUDAD_CHECK_SINGLE_PRECISION_FUNCTION_TYPE 1
#endif

#if CUDAD_CHECK_SINGLE_PRECISION_FUNCTION_TYPE==1
#define _CHECK_TYPE(T) static_assert(CUDAD_NAMESPACE internal::is_same_v<T, float>, "float-alias called on a datatype other than float, this usually indicates an error")
#else
#define _CHECK_TYPE(T) (void)0
#endif


CUDAD_NAMESPACE_BEGIN


template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> powf(const fvar<T, N, TDiff>& a, const S& b)
{
    _CHECK_TYPE(T);
    _CHECK_TYPE(S);
    return CUDAD_NAMESPACE pow(a, b);
}
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> powf(const S& a, const fvar<T, N, TDiff>& b)
{
    _CHECK_TYPE(T);
    _CHECK_TYPE(S);
    return CUDAD_NAMESPACE pow(a, b);
}
template <
    typename T, int N, typename TDiff>
    CUDAD_CALL fvar<T, N, TDiff> powf(const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b)
{
    _CHECK_TYPE(T);
    return CUDAD_NAMESPACE pow(a, b);
}

template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> fminf(const fvar<T, N, TDiff>& a, const S& b)
{
    _CHECK_TYPE(T);
    return CUDAD_NAMESPACE fmin(a, b);
}
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> fminf(const S& a, const fvar<T, N, TDiff>& b)
{
    _CHECK_TYPE(T);
    return CUDAD_NAMESPACE fmin(a, b);
}
template <
    typename T, int N, typename TDiff>
    CUDAD_CALL fvar<T, N, TDiff> fminf(const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b)
{
    _CHECK_TYPE(T);
    return CUDAD_NAMESPACE fmin(a, b);
}

template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> fmaxf(const fvar<T, N, TDiff>& a, const S& b)
{
    _CHECK_TYPE(T);
    return CUDAD_NAMESPACE fmax(a, b);
}
template <
    typename T, int N, typename TDiff,
    typename S, internal::enable_if_type_or_floating_t<S, T>* = nullptr>
    CUDAD_CALL fvar<T, N, TDiff> fmaxf(const S& a, const fvar<T, N, TDiff>& b)
{
    _CHECK_TYPE(T);
    return CUDAD_NAMESPACE fmax(a, b);
}
template <
    typename T, int N, typename TDiff>
    CUDAD_CALL fvar<T, N, TDiff> fmaxf(const fvar<T, N, TDiff>& a, const fvar<T, N, TDiff>& b)
{
    _CHECK_TYPE(T);
    return CUDAD_NAMESPACE fmax(a, b);
}

#define DECLARE_UNARY_OP(double_name, single_name)                          \
    template <typename T, int N, typename TDiff>                            \
    CUDAD_CALL fvar<T, N, TDiff> single_name(const fvar<T, N, TDiff>& v)    \
    {                                                                       \
        _CHECK_TYPE(T);                                                     \
        return CUDAD_NAMESPACE double_name(v);                              \
    }

DECLARE_UNARY_OP(fabs, fabsf)
DECLARE_UNARY_OP(exp, expf)
DECLARE_UNARY_OP(log, logf)
DECLARE_UNARY_OP(log2, log2f)
DECLARE_UNARY_OP(log10, log10f)
DECLARE_UNARY_OP(sqrt, sqrtf)
DECLARE_UNARY_OP(rsqrt, rsqrtf)
DECLARE_UNARY_OP(cbrt, cbrtf)

DECLARE_UNARY_OP(sin, sinf)
DECLARE_UNARY_OP(cos, cosf)
DECLARE_UNARY_OP(tan, tanf)
DECLARE_UNARY_OP(asin, asinf)
DECLARE_UNARY_OP(acos, acosf)
DECLARE_UNARY_OP(atan, atanf)
DECLARE_UNARY_OP(sinh, sinhf)
DECLARE_UNARY_OP(cosh, coshf)
DECLARE_UNARY_OP(tanh, tanhf)
DECLARE_UNARY_OP(asinh, asinhf)
DECLARE_UNARY_OP(acosh, acoshf)
DECLARE_UNARY_OP(atanh, atanhf)


CUDAD_NAMESPACE_END

#undef DECLARE_UNARY_OP
#undef _CHECK_TYPE

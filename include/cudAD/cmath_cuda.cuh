#pragma once
#ifndef __CUDAD_CMATH_CUDA_H__
#define __CUDAD_CMATH_CUDA_H__

#ifdef CUDA_NO_HOST

#if __has_include(<helper_math.cuh>)
#include <helper_math.cuh>
#define __CUDAD_CMATH_CUDA_AVAILABLE 1
#elif __has_include("helper_math.cuh")
#include "helper_math.cuh"
#define __CUDAD_CMATH_CUDA_AVAILABLE 1
#else
#warning "No helper_math.cuh found in include path, include it yourself before this file"
#endif

#ifdef __CUDAD_CMATH_CUDA_AVAILABLE
#include "macros.h"

namespace std
{
	
    CUDAD_CALL_NOCONSTEXPR float acos(float _Xx) {
        return :: acosf(_Xx);
	}

    CUDAD_CALL_NOCONSTEXPR float acosh(float _Xx) {
        return :: acoshf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float asin(float _Xx) {
        return :: asinf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float asinh(float _Xx) {
        return :: asinhf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float atan(float _Xx) {
        return :: atanf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float atanh(float _Xx) {
        return :: atanhf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float atan2(float _Yx, float _Xx) {
        return :: atan2f(_Yx, _Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float cbrt(float _Xx) {
        return :: cbrtf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float ceil(float _Xx) {
        return :: ceilf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float copysign(float _Number, float _Sign) {
        return :: copysignf(_Number, _Sign);
    }

    CUDAD_CALL_NOCONSTEXPR float cos(float _Xx) {
        return :: cosf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float cosh(float _Xx) {
        return :: coshf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float erf(float _Xx) {
        return :: erff(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float erfc(float _Xx) {
        return :: erfcf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float exp(float _Xx) {
        return :: expf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float exp2(float _Xx) {
        return :: exp2f(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float expm1(float _Xx) {
        return :: expm1f(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float fabs(float _Xx) {
        return :: fabsf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float fdim(float _Xx, float _Yx) {
        return :: fdimf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float floor(float _Xx) {
        return :: floorf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float fma(float _Xx, float _Yx, float _Zx) {
        return :: fmaf(_Xx, _Yx, _Zx);
    }

    CUDAD_CALL_NOCONSTEXPR float fmax(float _Xx, float _Yx) {
        return :: fmaxf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float fmin(float _Xx, float _Yx) {
        return :: fminf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float fmod(float _Xx, float _Yx) {
        return :: fmodf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float frexp(float _Xx, int* _Yx) {
        return :: frexpf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float hypot(float _Xx, float _Yx) {
        return :: hypotf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR int ilogb(float _Xx) {
        return :: ilogbf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float ldexp(float _Xx, int _Yx) {
        return :: ldexpf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float lgamma(float _Xx) {
        return :: lgammaf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR long long llrint(float _Xx) {
        return :: llrintf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR long long llround(float _Xx) {
        return :: llroundf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float log(float _Xx) {
        return :: logf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float log10(float _Xx) {
        return :: log10f(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float log1p(float _Xx) {
        return :: log1pf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float log2(float _Xx) {
        return :: log2f(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float logb(float _Xx) {
        return :: logbf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR long lrint(float _Xx) {
        return :: lrintf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR long lround(float _Xx) {
        return :: lroundf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float modf(float _Xx, float* _Yx) {
        return :: modff(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float nearbyint(float _Xx) {
        return :: nearbyintf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float nextafter(float _Xx, float _Yx) {
        return :: nextafterf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float pow(float _Xx, float _Yx) {
        return :: powf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float remainder(float _Xx, float _Yx) {
        return :: remainderf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float remquo(float _Xx, float _Yx, int* _Zx) {
        return :: remquof(_Xx, _Yx, _Zx);
    }

    CUDAD_CALL_NOCONSTEXPR float rint(float _Xx) {
        return :: rintf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float round(float _Xx) {
        return :: roundf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float scalbln(float _Xx, long _Yx) {
        return :: scalblnf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float scalbn(float _Xx, int _Yx) {
        return :: scalbnf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR float sin(float _Xx) {
        return :: sinf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float sinh(float _Xx) {
        return :: sinhf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float sqrt(float _Xx) {
        return :: sqrtf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float tan(float _Xx) {
        return :: tanf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float tanh(float _Xx) {
        return :: tanhf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float tgamma(float _Xx) {
        return :: tgammaf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR float trunc(float _Xx) {
        return :: truncf(_Xx);
    }



    //====================================
    // DOUBLE VERSION
    //====================================

    CUDAD_CALL_NOCONSTEXPR double acos(double _Xx) {
        return ::acos(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double acosh(double _Xx) {
        return ::acosh(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double asin(double _Xx) {
        return ::asin(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double asinh(double _Xx) {
        return ::asinh(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double atan(double _Xx) {
        return ::atan(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double atanh(double _Xx) {
        return ::atanh(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double atan2(double _Yx, double _Xx) {
        return ::atan2(_Yx, _Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double cbrt(double _Xx) {
        return ::cbrt(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double ceil(double _Xx) {
        return ::ceil(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double copysign(double _Number, double _Sign) {
        return ::copysign(_Number, _Sign);
    }

    CUDAD_CALL_NOCONSTEXPR double cos(double _Xx) {
        return ::cos(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double cosh(double _Xx) {
        return ::cosh(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double erf(double _Xx) {
        return ::erf(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double erfc(double _Xx) {
        return ::erfc(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double exp(double _Xx) {
        return ::exp(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double exp2(double _Xx) {
        return ::exp2(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double expm1(double _Xx) {
        return ::expm1(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double fabs(double _Xx) {
        return ::fabs(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double fdim(double _Xx, double _Yx) {
        return ::fdim(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double floor(double _Xx) {
        return ::floor(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double fma(double _Xx, double _Yx, double _Zx) {
        return ::fma(_Xx, _Yx, _Zx);
    }

    CUDAD_CALL_NOCONSTEXPR double fmax(double _Xx, double _Yx) {
        return ::fmax(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double fmin(double _Xx, double _Yx) {
        return ::fmin(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double fmod(double _Xx, double _Yx) {
        return ::fmod(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double frexp(double _Xx, int* _Yx) {
        return ::frexp(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double hypot(double _Xx, double _Yx) {
        return ::hypot(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR int ilogb(double _Xx) {
        return ::ilogb(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double ldexp(double _Xx, int _Yx) {
        return ::ldexp(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double lgamma(double _Xx) {
        return ::lgamma(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR long long llrint(double _Xx) {
        return ::llrint(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR long long llround(double _Xx) {
        return ::llround(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double log(double _Xx) {
        return ::log(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double log10(double _Xx) {
        return ::log10(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double log1p(double _Xx) {
        return ::log1p(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double log2(double _Xx) {
        return ::log2(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double logb(double _Xx) {
        return ::logb(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR long lrint(double _Xx) {
        return ::lrint(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR long lround(double _Xx) {
        return ::lround(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double modf(double _Xx, double* _Yx) {
        return ::modf(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double nearbyint(double _Xx) {
        return ::nearbyint(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double nextafter(double _Xx, double _Yx) {
        return ::nextafter(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double pow(double _Xx, double _Yx) {
        return ::pow(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double remainder(double _Xx, double _Yx) {
        return ::remainder(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double remquo(double _Xx, double _Yx, int* _Zx) {
        return ::remquo(_Xx, _Yx, _Zx);
    }

    CUDAD_CALL_NOCONSTEXPR double rint(double _Xx) {
        return ::rint(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double round(double _Xx) {
        return ::round(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double scalbln(double _Xx, long _Yx) {
        return ::scalbln(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double scalbn(double _Xx, int _Yx) {
        return ::scalbn(_Xx, _Yx);
    }

    CUDAD_CALL_NOCONSTEXPR double sin(double _Xx) {
        return ::sin(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double sinh(double _Xx) {
        return ::sinh(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double sqrt(double _Xx) {
        return ::sqrt(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double tan(double _Xx) {
        return ::tan(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double tanh(double _Xx) {
        return ::tanh(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double tgamma(double _Xx) {
        return ::tgamma(_Xx);
    }

    CUDAD_CALL_NOCONSTEXPR double trunc(double _Xx) {
        return ::trunc(_Xx);
    }
	
    //using :: abs;
    //using :: acos;
    //using :: asin;
    //using :: atan;
    //using :: atan2;
    //using :: ceil;
    //using :: cos;
    //using :: cosh;
    //using :: exp;
    //using :: fabs;
    //using :: floor;
    //using :: fmod;
    //using :: frexp;
    //using :: ldexp;
    //using :: log;
    //using :: log10;
    //using :: modf;
    //using :: pow;
    //using :: sin;
    //using :: sinh;
    //using :: sqrt;
    //using :: tan;
    //using :: tanh;

    //using :: acosh;
    //using :: asinh;
    //using :: atanh;
    //using :: cbrt;
    //using :: erf;
    //using :: erfc;
    //using :: expm1;
    //using :: exp2;
    //using :: hypot;
    //using :: ilogb;
    //using :: lgamma;
    //using :: log1p;
    //using :: log2;
    //using :: logb;
    //using :: llrint;
    //using :: lrint;
    //using :: nearbyint;
    //using :: rint;
    //using :: llround;
    //using :: lround;
    //using :: fdim;
    //using :: fma;
    //using :: fmax;
    //using :: fmin;
    //using :: round;
    //using :: trunc;
    //using :: remainder;
    //using :: remquo;
    //using :: copysign;
    //using :: nan;
    //using :: nextafter;
    //using :: scalbn;
    //using :: scalbln;
    //using :: tgamma;
}

#endif
#endif
#endif

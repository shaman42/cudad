#include <catch/catch.hpp>
#include <cudAD/forward.h>

using namespace cudAD;

TEST_CASE("Loop-Add", "[ops]")
{
    typedef fvar<float, 1> f1;
    const f1 input = f1::input<0>(3);

    int k = 10;
    f1 output1 = f1::constant(0);
    for (int i = 0; i < k; ++i)
        output1 += input;

    f1 output2 = input * k;

    REQUIRE(output1.value() == Approx(output2.value()));
    REQUIRE(output1.derivative<0>() == Approx(output2.derivative<0>()));
}

TEST_CASE("Loop-Mul", "[ops]")
{
    typedef fvar<float, 1> f1;
    const f1 input = f1::input<0>(3);

    int k = 10;
    f1 output1 = f1::constant(1);
    for (int i = 0; i < k; ++i)
        output1 *= input;

    f1 output2 = pow(input, k);

    REQUIRE(output1.value() == Approx(output2.value()));
    REQUIRE(output1.derivative<0>() == Approx(output2.derivative<0>()));
}

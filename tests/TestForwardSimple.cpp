#include <catch/catch.hpp>
#include <cudAD/forward.h>

using namespace cudAD;

// high-level test
TEST_CASE("create variables", "[core]")
{
    typedef fvar<float, 5> f5;
    SECTION("k=0")
    {
        f5 var = f5::input<0>(42);
        INFO("var=" << var);
        REQUIRE(var.value() == 42);
        for (int i = 0; i < f5::NumDerivatives; ++i)
            if (i == 0)
                REQUIRE(var.derivative(i) == 1);
            else
                REQUIRE(var.derivative(i) == 0);
    }
    SECTION("k=1")
    {
        f5 var = f5::input<1>(42);
        INFO("var=" << var);
        REQUIRE(var.value() == 42);
        for (int i = 0; i < f5::NumDerivatives; ++i)
            if (i == 1)
                REQUIRE(var.derivative(i) == 1);
            else
                REQUIRE(var.derivative(i) == 0);
    }
    SECTION("k=2")
    {
        f5 var = f5::input<2>(42);
        INFO("var=" << var);
        REQUIRE(var.value() == 42);
        for (int i = 0; i < f5::NumDerivatives; ++i)
            if (i == 2)
                REQUIRE(var.derivative(i) == 1);
            else
                REQUIRE(var.derivative(i) == 0);
    }
}

TEST_CASE("copy-assign", "[core]")
{
    typedef fvar<float, 5> f5;
    const auto check = [](const f5& var, float value, float d0, float d1, float d2, float d3, float d4)
    {
        INFO("var=" << var);
        REQUIRE(var.value() == value);
        REQUIRE(static_cast<float>(var) == value);
        REQUIRE(var.derivative<0>() == d0);
        REQUIRE(var.derivative<1>() == d1);
        REQUIRE(var.derivative<2>() == d2);
        REQUIRE(var.derivative<3>() == d3);
        REQUIRE(var.derivative<4>() == d4);
    };

    f5 var1, var2;
    check(var1, 0, 0, 0, 0, 0, 0);
    check(var2, 0, 0, 0, 0, 0, 0);

    var1 = f5::input<2>(11);
    check(var1, 11, 0, 0, 1, 0, 0);
    var2 = var1;
    check(var2, 11, 0, 0, 1, 0, 0);

    var2 = f5::constant(22);
    check(var2, 22, 0, 0, 0, 0, 0);
    check(var1, 11, 0, 0, 1, 0, 0);
}

TEST_CASE("No derivatives", "[core]")
{
    typedef fvar<float, 0> f0;

    f0 a = f0::constant(42);
    f0 b = f0::constant(43);
    f0 c = a + b;
    REQUIRE(c.value() == 42 + 43);
    REQUIRE_THROWS(c.derivatives()[0]);
}

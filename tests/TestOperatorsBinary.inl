// define OP(a,b) then include this file again
// define POS_A or POS_B if those two variables can only be positive

do {
    const auto testOp = []()
    {
        static int N = 100;
        static const double epsilon = 1e-8;
        static const double tolerance = 1e-4;

        typedef fvar<double, 1> d1;
        typedef fvar<double, 2> d2;

        std::normal_distribution<double> distr;
        for (int i = 0; i < N; ++i)
        {
            double v1 = distr(rnd);
            double v2 = distr(rnd);
#ifdef POS_A
            v1 = std::abs(v1);
#endif
#ifdef POS_B
            v2 = std::abs(v2);
#endif
            INFO("i=" << i << ", v1=" << v1 << ", v2=" << v2);

            //target, numerical derivative
            double vr = OP(v1, v2);
            double numD1 = (OP(v1 + epsilon, v2) - vr) / epsilon;
            double numD2 = (OP(v1, v2 + epsilon) - vr) / epsilon;
            double numDb = (OP(v1 + epsilon, v2 + epsilon) - vr) / epsilon;

            //forward derivatives
            d1 var1a = d1::input<0>(v1);
            d1 var2a = d1::input<0>(v2);
            {
                d1 res = OP(var1a, v2);
                REQUIRE(res.value() == vr);
                REQUIRE(res.derivative(0) == Approx(numD1).epsilon(tolerance).margin(tolerance));
            }
            {
                d1 res = OP(v1, var2a);
                REQUIRE(res.value() == vr);
                REQUIRE(res.derivative(0) == Approx(numD2).epsilon(tolerance).margin(tolerance));
            }
            {
                d1 res = OP(var1a, var2a);
                REQUIRE(res.value() == vr);
                REQUIRE(res.derivative(0) == Approx(numDb).epsilon(tolerance).margin(tolerance));
            }

            d2 var1b = d2::input<0>(v1);
            d2 var2b = d2::input<1>(v2);
            {
                d2 res = OP(var1b, v2);
                REQUIRE(res.value() == vr);
                REQUIRE(res.derivative(0) == Approx(numD1).epsilon(tolerance).margin(tolerance));
                REQUIRE(res.derivative(1) == 0);
            }
            {
                d2 res = OP(v1, var2b);
                REQUIRE(res.value() == vr);
                REQUIRE(res.derivative(1) == Approx(numD2).epsilon(tolerance).margin(tolerance));
                REQUIRE(res.derivative(0) == 0);
            }
            {
                d2 res = OP(var1b, var2b);
                REQUIRE(res.value() == vr);
                REQUIRE(res.derivative(0) == Approx(numD1).epsilon(tolerance).margin(tolerance));
                REQUIRE(res.derivative(1) == Approx(numD2).epsilon(tolerance).margin(tolerance));
            }
        }
    };
    testOp();
} while (0);

#include <catch/catch.hpp>
#include <cudAD/forward.h>
#include <cudAD/optimization.h>

#include <sstream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace cudAD;

TEST_CASE("Rosenbrock", "[optim]")
{
    typedef double Scalar;
    const Scalar a = 1;
    const Scalar b = 100;
    const int numIterations = 100;
    const Scalar minGradientNorm = 1e-7;
    const Scalar epsilon = 1e-5;

    //Rosenbrock function
    const Optimizer<Scalar>::Fun_t function =
        [a, b]
        (const Optimizer<Scalar>::Vector_t& input, Optimizer<Scalar>::Vector_t& grad)
    {
        typedef fvar<Scalar, 2> f2;
        f2 x = f2::input<0>(input[0]);
        f2 y = f2::input<1>(input[1]);
        const auto v1 = a - x;
        const auto v2 = y - x * x;
        const auto result = v1 * v1 + b * v2 * v2;
        grad[0] = result.derivative<0>();
        grad[1] = result.derivative<1>();
        return result.value();
    };

    //global optimimum
    Optimizer<Scalar>::Vector_t optimum{ a, a * a };

    //create optimizer
    GradientDescent<Scalar> optim(function, 2);
    optim.setMaxIterations(numIterations);
    optim.setMinGradientNorm(minGradientNorm);

    //run optimizer
    Optimizer<Scalar>::Vector_t initial{ -0.5, 0.5 };
    optim.init(initial);
    std::stringstream out;
    out << "k=0 -> " << initial[0] << " " << initial[1] << std::endl;
    do
    {
        optim.step();
        auto diff = optimum - optim.currentX();
        Scalar distance = std::sqrt((diff * diff).sum());
        out << "k=" << optim.currentIteration() <<
            "  f(" << optim.currentX()[0] << ", " << optim.currentX()[1] <<
            ") = " << optim.currentValue() << "; grad = (" <<
            optim.currentGrad()[0] << ", " << optim.currentGrad()[1] <<
            "); distance to optimum: " << distance << std::endl;
    } while (!optim.checkTermination());
    out << "Done" << std::endl;

    INFO(out.str());
    REQUIRE(optim.currentIteration() < numIterations); //converged by norm, not by iteration
    REQUIRE(optim.currentX()[0] == Approx(optimum[0]).epsilon(epsilon));
    REQUIRE(optim.currentX()[1] == Approx(optimum[1]).epsilon(epsilon));
}

TEST_CASE("Easom", "[optim][!hide]") //TODO: gradient descent can't solve that
{
    typedef double Scalar;
    const int numIterations = 100;
    const Scalar minGradientNorm = -1;
    const Scalar epsilon = 1e-5;

    //Easom function
    const Optimizer<Scalar>::Fun_t function = []
    (const Optimizer<Scalar>::Vector_t& input, Optimizer<Scalar>::Vector_t& grad)
    {
        typedef fvar<Scalar, 2> f2;
        f2 x = f2::input<0>(input[0]);
        f2 y = f2::input<1>(input[1]);
        const auto v1 = x - M_PI;
        const auto v2 = y - M_PI;
        const auto result = -cos(x) * cos(y) * exp(
            -(v1 * v1 + v2 * v2));
        grad[0] = result.derivative<0>();
        grad[1] = result.derivative<1>();
        return result.value();
    };

    //global optimimum
    Optimizer<Scalar>::Vector_t optimum{ M_PI, M_PI };

    //create optimizer
    GradientDescent<Scalar> optim(function, 2, 1e4);
    optim.setMaxIterations(numIterations);
    optim.setMinGradientNorm(minGradientNorm);

    //run optimizer
    Optimizer<Scalar>::Vector_t initial{ 0.5, 1 };
    optim.init(initial);
    std::stringstream out;
    out << "k=0 -> " << initial[0] << " " << initial[1] << std::endl;
    do
    {
        optim.step();
        auto diff = optimum - optim.currentX();
        Scalar distance = std::sqrt((diff * diff).sum());
        out << "k=" << optim.currentIteration() <<
            "  f(" << optim.currentX()[0] << ", " << optim.currentX()[1] <<
            ") = " << optim.currentValue() << "; grad = (" <<
            optim.currentGrad()[0] << ", " << optim.currentGrad()[1] <<
            "); distance to optimum: " << distance << std::endl;
    } while (!optim.checkTermination());
    out << "Done" << std::endl;

    INFO(out.str());
    REQUIRE(optim.currentIteration() < numIterations); //converged by norm, not by iteration
    REQUIRE(optim.currentX()[0] == Approx(optimum[0]).epsilon(epsilon));
    REQUIRE(optim.currentX()[1] == Approx(optimum[1]).epsilon(epsilon));
}


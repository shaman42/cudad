// define OP(a,b) then include this file again
// define POS_A or POS_B if those two variables can only be positive

const auto testUnary = [](double min=-DBL_MAX, double max=DBL_MAX)
{
    static int N = 100;
    static const double epsilon = 1e-8;
    static const double tolerance = 1e-4;

    typedef fvar<double, 1> d1;

    std::normal_distribution<double> distr;
    for (int i = 0; i < N; ++i)
    {
        double v1;
    	do
    	{
            v1 = distr(rnd);
        } while (min >= v1 || v1 >= max);
        INFO("i=" << i << ", v1=" << v1);

        //target, numerical derivative
        double vr = OP(v1);
        double numD1 = (OP(v1 + epsilon) - vr) / epsilon;

        //forward derivatives
        d1 var1 = d1::input<0>(v1);
        d1 res = OP(var1);
        REQUIRE(res.value() == vr);
        REQUIRE(res.derivative(0) == Approx(numD1).epsilon(tolerance).margin(tolerance));
    }
};

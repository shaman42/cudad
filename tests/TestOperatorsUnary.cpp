#include <catch/catch.hpp>
#include <cudAD/forward.h>

#include <random>
#include <cfloat>

using namespace cudAD;
using namespace std;

static std::default_random_engine rnd;

TEST_CASE("basic unary", "[ops]")
{
    SECTION("abs") {
        //I have to include everything here.
        //abs(x) by default calls the integer-version even for x being a double
        const auto testUnary = [](double min = -DBL_MAX, double max = DBL_MAX)
        {
            static int N = 100;
            static const double epsilon = 1e-8;
            static const double tolerance = 1e-4;

            typedef fvar<double, 1> d1;

            std::normal_distribution<double> distr;
            for (int i = 0; i < N; ++i)
            {
                double v1;
                do
                {
                    v1 = distr(rnd);
                } while (min >= v1 || v1 >= max);
                INFO("i=" << i << ", v1=" << v1);

                //target, numerical derivative
                double vr = fabs(v1);
                double numD1 = (fabs(v1 + epsilon) - vr) / epsilon;

                //forward derivatives
                d1 var1 = d1::input<0>(v1);
                d1 res = abs(var1);
                REQUIRE(res.value() == vr);
                REQUIRE(res.derivative(0) == Approx(numD1).epsilon(tolerance).margin(tolerance));
            }
        };
        testUnary();
    }

    SECTION("exp") {
#define OP(a) (exp((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("log") {
#define OP(a) (log((a)))
#include "TestOperatorsUnary.inl"
        testUnary(FLT_MIN, FLT_MAX); //only positive
#undef OP
    }

    SECTION("log2") {
#define OP(a) (log2((a)))
#include "TestOperatorsUnary.inl"
        testUnary(FLT_MIN, FLT_MAX); //only positive
#undef OP
    }

    SECTION("log10") {
#define OP(a) (log10((a)))
#include "TestOperatorsUnary.inl"
        testUnary(FLT_MIN, FLT_MAX); //only positive
#undef OP
    }

    SECTION("sqrt") {
#define OP(a) (sqrt((a)))
#include "TestOperatorsUnary.inl"
        testUnary(FLT_MIN, FLT_MAX); //only positive
#undef OP
    }

    SECTION("cbrt") {
#define OP(a) (cbrt((a)))
#include "TestOperatorsUnary.inl"
        testUnary(FLT_MIN, FLT_MAX); //only positive
#undef OP
    }

    SECTION("sin") {
#define OP(a) (sin((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("cos") {
#define OP(a) (cos((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("tan") {
#define OP(a) (tan((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("asin") {
#define OP(a) (asin((a)))
#include "TestOperatorsUnary.inl"
        testUnary(-1, +1);
#undef OP
    }

    SECTION("acos") {
#define OP(a) (acos((a)))
#include "TestOperatorsUnary.inl"
        testUnary(-1, +1);
#undef OP
    }

    SECTION("atan") {
#define OP(a) (atan((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("sinh") {
#define OP(a) (sinh((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("cosh") {
#define OP(a) (cosh((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("tanh") {
#define OP(a) (tanh((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("asinh") {
#define OP(a) (asinh((a)))
#include "TestOperatorsUnary.inl"
        testUnary();
#undef OP
    }

    SECTION("acosh") {
#define OP(a) (acosh((a)))
#include "TestOperatorsUnary.inl"
        testUnary(1, FLT_MAX);
#undef OP
    }

    SECTION("atanh") {
#define OP(a) (atanh((a)))
#include "TestOperatorsUnary.inl"
        testUnary(-1, +1);
#undef OP
    }
}

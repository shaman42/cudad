#include <catch/catch.hpp>
#include <cudAD/forward.h>
#include <cudAD/forward_vector.h>
#include <cudAD/optimization.h>

#include <sstream>

using namespace cudAD;

namespace std
{
    ostream& operator<<(ostream& o, float3 var)
    {
        return o << "[" << var.x << "," << var.y << "," << var.z << "]";
    }
}

TEST_CASE("float3-simple", "[vector]")
{
    typedef fvar<float3, 3> v3;

    v3 var1 = make_float3in<3>(2, 4, 3);
    v3 const1 = v3::constant(make_float3(3, 0, -1));

    v3 out1 = var1 * const1;

    REQUIRE(out1.value().x == 2 * 3);
    REQUIRE(out1.value().y == 4 * 0);
    REQUIRE(out1.value().z == 3 *-1);
    REQUIRE(all(out1.derivative<0>() == make_float3(3, 0, 0)));
    REQUIRE(all(out1.derivative<1>() == make_float3(0, 0, 0)));
    REQUIRE(all(out1.derivative<2>() == make_float3(0, 0,-1)));
}

TEST_CASE("float3-optim", "[vector]")
{
    //optimize for length and angle
    typedef fvar<float3, 3> v3;
    const float3 initial = make_float3(-1, 0.5, 1.7);
    const float3 target = make_float3(0.3, -0.6, 0.2);
    const int numIterations = 100;
    const float minGradientNorm = 1e-9;
    const float epsilon = 1e-3;

    const Optimizer<float>::Fun_t function = [target]
    (const Optimizer<float>::Vector_t& input, Optimizer<float>::Vector_t& grad) -> float
    {
        v3 in = make_float3in<3>(input[0], input[1], input[2]);
        //length difference
        auto lengthIn = length(in);
        auto lengthTarget = length(target);
        auto lossLength = square(lengthIn - lengthTarget);
        //cosine similarity
        auto similarity = dot(in, target) / (lengthIn * lengthTarget);
        auto lossAngle = square(1 - similarity);
        //final loss
        auto loss = lossLength + lossAngle;
        grad[0] = loss.derivative<0>();
        grad[1] = loss.derivative<1>();
        grad[2] = loss.derivative<2>();
        return loss.value();
    };

    //create optimizer
    GradientDescent<float> optim(function, 3);
    optim.setMaxIterations(numIterations);
    optim.setMinGradientNorm(minGradientNorm);

    //run optimizer
    optim.init({initial.x, initial.y, initial.z});
    std::stringstream out;
    out << "target: " << target.x << ", " << target.y << ", " << target.z << std::endl;
    out << "k=0 -> " << initial.x << ", " << initial.y << ", " << initial.z << std::endl;
    do
    {
        optim.step();
        out << "k=" << optim.currentIteration() <<
            "  f(" << optim.currentX()[0] << ", " << optim.currentX()[1] << ", " << optim.currentX()[2] <<
            ") = " << optim.currentValue() << "; grad = (" <<
            optim.currentGrad()[0] << ", " << optim.currentGrad()[1] << ", " << optim.currentGrad()[2] <<
            ")" << std::endl;
    } while (!optim.checkTermination());
    out << "Done" << std::endl;

    INFO(out.str());
    REQUIRE(optim.currentIteration() < numIterations); //converged by norm, not by iteration
    REQUIRE(optim.currentX()[0] == Approx(target.x).epsilon(epsilon));
    REQUIRE(optim.currentX()[1] == Approx(target.y).epsilon(epsilon));
    REQUIRE(optim.currentX()[2] == Approx(target.z).epsilon(epsilon));
}

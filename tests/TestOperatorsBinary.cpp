#include <catch/catch.hpp>
#include <cudAD/forward.h>

#include <random>
#include <algorithm>

using namespace cudAD;

static std::default_random_engine rnd;

TEST_CASE("basic binary", "[ops]")
{
    typedef fvar<float, 1> f1;
    const auto check = [](const f1& var, float value, float d0)
    {
        INFO("var=" << var);
        REQUIRE(var.value() == value);
        REQUIRE(var.derivative<0>() == d0);
    };

    SECTION("add") {
#define OP(a, b) ((a) + (b))
#include "TestOperatorsBinary.inl"
#undef OP
    }

    SECTION("sub") {
#define OP(a, b) ((a) - (b))
#include "TestOperatorsBinary.inl"
#undef OP
    }

    SECTION("mul") {
#define OP(a, b) ((a) * (b))
#include "TestOperatorsBinary.inl"
#undef OP
    }

    SECTION("div") {
#define OP(a, b) ((a) / (b))
#include "TestOperatorsBinary.inl"
#undef OP
    }

    SECTION("pow") {
#define OP(a, b) (pow((a), (b)))
#define POS_A 1
#include "TestOperatorsBinary.inl"
#undef POS_A
#undef OP
    }

    SECTION("min") {
        using std::fmin;
#define OP(a, b) (fmin((a), (b)))
#include "TestOperatorsBinary.inl"
#undef OP
    }

    SECTION("max") {
        using std::fmax;
#define OP(a, b) (fmax((a), (b)))
#include "TestOperatorsBinary.inl"
#undef OP
    }
}

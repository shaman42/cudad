#include <iostream>
#include <assert.h>
#include "cuda_runtime.h"
#include <helper_math.cuh>
#include <cudAD/forward.h>
#include <cudAD/forward_vector.h>

using namespace cudAD;
using namespace std;

__global__ void TestKernel(float4 inputValue, float4 inputGradient)
{
    internal::array<float4, 1> inputGradientA; inputGradientA[0] = inputGradient;
    fvar<float4, 1> input(inputValue, inputGradientA);

    float4 reference = make_float4(0.566, 0.329, 0.270, 0.528);

    fvar<float, 1> output = length(input - reference);
    printf("Output: %.4f [%.4f]\n", output.value(), output.derivative(0));
}

int main()
{
    float4 inputValue = make_float4(0.566, 0.329, 0.270, 0.528);
    float4 inputGradient = make_float4(-660.272, -378.419, -308.077, -615.864);
    TestKernel <<< 1, 1 >>> (inputValue, inputGradient);
    auto result = cudaDeviceSynchronize();
    if (cudaSuccess != result) {
        std::cerr << "Error!: 0" << cudaGetErrorString(result) << std::endl;
    }
}
